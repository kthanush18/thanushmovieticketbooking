package com.catalog;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.booking.Movie;

public class Catalog {
	private Date lastUpdated;
	private Map<String, List<Movie>> movieTitles;
	private Map<String, List<Movie>> movieLanguages;
	private Map<String, List<Movie>> movieGenres;
	private Map<String, List<Movie>> movieReleaseDates;
	private Map<String, List<Movie>> movieCities;
	
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public Map<String, List<Movie>> getMovieTitles() {
		return movieTitles;
	}
	public void setMovieTitles(Map<String, List<Movie>> movieTitles) {
		this.movieTitles = movieTitles;
	}
	public Map<String, List<Movie>> getMovieLanguages() {
		return movieLanguages;
	}
	public void setMovieLanguages(Map<String, List<Movie>> movieLanguages) {
		this.movieLanguages = movieLanguages;
	}
	public Map<String, List<Movie>> getMovieGenres() {
		return movieGenres;
	}
	public void setMovieGenres(Map<String, List<Movie>> movieGenres) {
		this.movieGenres = movieGenres;
	}
	public Map<String, List<Movie>> getMovieReleaseDates() {
		return movieReleaseDates;
	}
	public void setMovieReleaseDates(Map<String, List<Movie>> movieReleaseDates) {
		this.movieReleaseDates = movieReleaseDates;
	}
	public Map<String, List<Movie>> getMovieCities() {
		return movieCities;
	}
	public void setMovieCities(Map<String, List<Movie>> movieCities) {
		this.movieCities = movieCities;
	}
	
}
