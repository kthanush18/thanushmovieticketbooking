package com.search;

public interface Search {
	
	public void SearchByTitle();
	public void SearchByLanguage();
	public void SearchByGenre();
	public void SearchByReleaseDate();
	public void SearchByCity();

}
