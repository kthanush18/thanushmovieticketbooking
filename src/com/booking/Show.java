package com.booking;

import java.time.ZonedDateTime;
import java.util.Date;

public class Show {
	private Date createdOn;
	private ZonedDateTime starTime;
	private ZonedDateTime endTime;
	
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public ZonedDateTime getStarTime() {
		return starTime;
	}
	public void setStarTime(ZonedDateTime starTime) {
		this.starTime = starTime;
	}
	public ZonedDateTime getEndTime() {
		return endTime;
	}
	public void setEndTime(ZonedDateTime endTime) {
		this.endTime = endTime;
	}

}
