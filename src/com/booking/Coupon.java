package com.booking;

import java.time.ZonedDateTime;

public class Coupon {
	private int id;
	private double balance;
	private ZonedDateTime expiry;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public ZonedDateTime getExpiry() {
		return expiry;
	}
	public void setExpiry(ZonedDateTime expiry) {
		this.expiry = expiry;
	}
	
	

}
