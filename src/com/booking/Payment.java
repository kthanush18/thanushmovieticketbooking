package com.booking;

import java.util.Date;

import com.status.BookingStatus;

public class Payment {
	
	private double amount;
	private Date createdOn;
	private BookingStatus paymentStatus;
	private int transactionID;
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public BookingStatus getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(BookingStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public int getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(int transactionID) {
		this.transactionID = transactionID;
	}
	
}
