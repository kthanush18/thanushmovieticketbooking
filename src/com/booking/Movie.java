package com.booking;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class Movie {
	private String title;	
	private String description;
	private int durationInMins;
	private String language;
	private ZonedDateTime releaseDate;
	private String country;
	private String genre;
	
	public String getTittle() {
		return title;
	}
	public void setTittle(String tittle) {
		this.title = tittle;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getDurationInMins() {
		return durationInMins;
	}
	public void setDurationInMins(int durationInMins) {
		this.durationInMins = durationInMins;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public ZonedDateTime getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(ZonedDateTime releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}

	public List<Show> getShows(){
		List<Show> shows = new ArrayList<Show>();
		return shows;
	}
}
