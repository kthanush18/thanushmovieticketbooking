package com.status;

public enum BookingStatus {
	Requested, Pending, Confirmed, Checked_in, Cancelled, Abandoned

}
