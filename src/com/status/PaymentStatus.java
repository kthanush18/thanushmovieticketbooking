package com.status;

public enum PaymentStatus {
	Unpaid,Pending,Completed,Failed,Declined,Cancelled,Abandoned,Settling,Settled,Refunded
}
